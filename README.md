# Test task for React Native
- The app should load currencies list when opening use this request: 
    >http://www.nbrb.by/API/ExRates/Currencies

    [Documentation](http://www.nbrb.by/APIHelp/ExRates)
    Use FlatList for render currencies list which should have modal mode.

- The logo should be animated, animation depends on showing input keyboard and work with keyboard lifecycles, logo sizes shouldn't be hardcoded.
- The keyboard should be closing by the tap on any place.
- All fields (name, currency, date) and button need to have validation.
- By pressing “Get rate” button app should send the request like this:
    >http://www.nbrb.by/API/ExRates/Rates/298?onDate=2016-7-5

    to get the current rate and show the result on the next screen.
- Going back should work only by horizontal swiping by two fingers.
- Libs which you can use, of course, you can use more libs if you want:
    ```sh
    "dependencies": {
       "react": "16.8.3",
       "react-native": "0.59.2",
       "react-native-gesture-handler": "^1.1.0",
       "react-native-modal-datetime-picker": "^6.1.0",
       "react-navigation": "^3.5.1"
     }
    ```